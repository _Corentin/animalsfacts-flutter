import 'package:animals_facts/models/fact.dart';

class Facts {

  List<Fact> _facts;

  Facts() {
    this._facts = <Fact>[];
  }

  factory Facts.fromJson(Map<String, dynamic> data) {
    Facts result = Facts();
    result._facts = (data["all"] as List).map((m) => new Fact.fromJson(m)).toList();
    return result;
  }

  List<Fact> facts() {
    return _facts;
  }
}