class Fact {

  final String text;
  final String animalType;

  Fact({this.text, this.animalType});

  factory Fact.fromJson(Map<String, dynamic> json) {
    print("Fact: " + json.toString());
    return Fact(
      text: json['text'],
      animalType: json['type']
    );
  }
}