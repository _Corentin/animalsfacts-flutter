import 'package:flutter/material.dart';

import 'package:animals_facts/animations/routes.dart';
import 'package:animals_facts/pages/fact_list.dart';

class AnimalsListPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _AnimalsListPageState();
}

class _AnimalsListPageState extends State<AnimalsListPage> {

  void openFacts(String animalType) {
    Navigator.push(
        context,
        FromBottomRoute(widget: FactListPage(animalType))
    );
  }

  @override
  Widget build(BuildContext context) {
    final _widgets = <Widget>[];
    _widgets.add(ListTile(
      title: Text("Cat"),
      onTap: () {
        openFacts("cat");
      }
    ));
    _widgets.add(Divider(height: 1.0, color: Colors.green));
    _widgets.add(ListTile(
      title: Text("Dog"),
      onTap: () {
        openFacts("dog");
      },
    ));
    _widgets.add(Divider(height: 1.0, color: Colors.green));
    _widgets.add(ListTile(
      title: Text("Horse"),
      onTap: () {
        openFacts("horse");
      },
    ));
    _widgets.add(Divider(height: 1.0, color: Colors.green));

    return Scaffold(
      appBar: AppBar(
        title: Text('Animals'),
      ),
      body: ListView(
        children: _widgets,
      )
    );
  }
}