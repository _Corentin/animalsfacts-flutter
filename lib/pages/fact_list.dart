import 'package:flutter/material.dart';

import 'package:animals_facts/models/fact.dart';
import 'package:animals_facts/models/facts.dart';
import 'package:animals_facts/services/animals_services.dart';

class FactListPage extends StatefulWidget {

  String _animalType;

  FactListPage(String animalType) {
    this._animalType = animalType;
  }

  @override
  State<StatefulWidget> createState() => _FactListPageState(this._animalType);
}

class _FactListPageState extends State<FactListPage> {

  String _animalType;

  _FactListPageState(String animalType) {
    this._animalType = animalType;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.close),
            onPressed: (){ Navigator.pop(context, true); }),
        title: Text('Facts'),
      ),
      body: Center(
        child: FutureBuilder<Facts>(
          future: AnimalsServices.facts(_animalType),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                final _widgets = <Widget>[];
                for (var fact in snapshot.data.facts()) {
                  _widgets.add(ListTile(title: Text(fact.text)));
                  _widgets.add(Divider(height: 1.0, color: Colors.green,));
                }

                return ListView(
                  children: _widgets,
                );
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }

              // By default, show a loading spinner
              return CircularProgressIndicator();
            },
          ),
        ),
      );
  }
}