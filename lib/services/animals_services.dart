import 'dart:async';
import 'dart:convert';

import 'package:animals_facts/models/facts.dart';
import 'package:http/http.dart' as http;

class AnimalsServices {
  static Future<Facts> facts(String animal) async {
    final response =
    await http.get('https://cat-fact.herokuapp.com/facts?animal_type=$animal');

    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON
      return Facts.fromJson(json.decode(response.body));
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load fact');
    }
  }
}